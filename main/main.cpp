#include <iostream>
#include <Windows.h>
#include "Personne.h"
#include "EquipeMoto.h"
using namespace std;

void AffichePersonne(Personne& p) {
    cout << "--------------------------------" << endl;
    cout << "Personne(" << &p << "): " << p.GetNom() << endl;
    cout << "--------------------------------" << endl;
}
void AfficheEquipe(EquipeMoto* equipe) {
    cout << "Adresse de l'équipe: " << equipe << endl;
    cout << "Nom de l'équipe: " << equipe->getNom() << endl;
    cout << "Nom du manager: " << equipe->getManager()->GetNom() << endl;
    Personne** pilotes = equipe->GetPilotes();
    for (unsigned int i = 0; i < EquipeMoto::maxPilote; ++i) {
        if (pilotes[i] != nullptr) {
            cout << "Pilote " << i + 1 << ": " << pilotes[i]->GetNom() << endl;
        }
        else {
            cout << "Pilote " << i + 1 << ": Pas de pilote" << endl;
        }
    }
}
Personne* CreerPersonne() {
    string nom;
    cout << "Entrez le nom de la personne: ";
    cin >> nom;

    Personne* nouvellePersonne = new Personne(nom);

    return nouvellePersonne;
}

int main()
{
	SetConsoleOutputCP(CP_UTF8);
   
	Personne pilote_1("");
	pilote_1.SetNom("Fabio");

    Personne* pilote_2 = CreerPersonne();

    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");

    equipe_1->AddPilote(0, new Personne(pilote_1));
    equipe_1->AddPilote(1, new Personne(*pilote_2));
    
    Personne pil_pramac_0("");
    pilote_1.SetNom("Flo");
    Personne pil_pramac_2("");
    pilote_1.SetNom("Max");

    EquipeMoto pramac = *equipe_1;
    pramac.setNom("Pramac");


    cout << "--------- Affichage des pilotes ---------" << endl;
    AffichePersonne(pilote_1);
    AffichePersonne(*pilote_2);

    cout << "--------- Affichage des Equipes ---------" << endl;
    AfficheEquipe(equipe_1);


    cout << "--------- Zone de supression ------------" << endl;
    delete pilote_2;
    delete equipe_1;
}