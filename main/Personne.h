#pragma once
#include <string>

using namespace std;
class Personne
{
private:
	string nom;
public:
	Personne(string nom);
	virtual ~Personne();

	string GetNom();

	void SetNom(string nom);
	
	

};

