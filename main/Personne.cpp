#include "Personne.h"
#include <iostream>
using namespace std;

Personne::Personne(string nom)
{
	this->nom = nom;
}

Personne::~Personne()
{
	cout << "Destruteur appel�: " << this << " [Nom: " << this->nom << "]" << endl;
}

string Personne::GetNom()
{
	return nom;
}

void Personne::SetNom(string nom)
{
	this->nom = nom;
}

