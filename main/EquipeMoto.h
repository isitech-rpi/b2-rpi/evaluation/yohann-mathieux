#pragma once
#include "Personne.h"
#include <string>


class EquipeMoto
{
public:
	static const unsigned int maxPilote = 3;

	EquipeMoto(string nomEquipe, string nomManager);

	~EquipeMoto();

	string getNom();
	void setNom(const string& nom);

	Personne* getManager() const;

	void AddPilote(unsigned int rang, Personne* pilote);

	Personne** GetPilotes();
private:
	string nom;
	
	Personne* manager;
	
	Personne* lesPilotes[maxPilote];
};

