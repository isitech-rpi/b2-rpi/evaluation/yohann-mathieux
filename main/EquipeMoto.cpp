#include "EquipeMoto.h"
#include "Personne.h"

EquipeMoto::EquipeMoto(string nomEquipe, string nomManager) : nom(nomEquipe) {
    manager = new Personne(nomManager);
    for (unsigned int i = 0; i < maxPilote; ++i) {
        lesPilotes[i] = nullptr;
    }
}

EquipeMoto::~EquipeMoto() {
    delete manager;
    for (unsigned int i = 0; i < maxPilote; ++i) {
        lesPilotes[i] = nullptr;
    }
}

string EquipeMoto::getNom()
{
    return nom;
}

void EquipeMoto::setNom(const string& nom)
{
    this->nom = nom;
}
Personne* EquipeMoto::getManager() const {
    return manager;
}

void EquipeMoto::AddPilote(unsigned int rang, Personne* pilote)
{
if (rang < maxPilote) {
        delete lesPilotes[rang];  
        lesPilotes[rang] = pilote;
    }
}

Personne** EquipeMoto::GetPilotes() {
    return lesPilotes;
}